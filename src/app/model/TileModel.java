package app.model;

import app.enums.TileEnum;
import app.enums.Treasure;

import java.util.List;

class TileModel {
    private List<PlayerModel> playerModelList;
    private TileEnum tile;
    private Treasure treasure;

    public List<PlayerModel> getPlayerList() {
        return playerModelList;
    }

    public void setPlayerList(List<PlayerModel> playerModelList) {
        this.playerModelList = playerModelList;
    }

    public TileEnum getTile() {
        return tile;
    }

    public void setTile(TileEnum tile) {
        this.tile = tile;
    }

    public Treasure getTreasure() {
        return treasure;
    }

    public void setTreasure(Treasure treasure) {
        this.treasure = treasure;
    }
}