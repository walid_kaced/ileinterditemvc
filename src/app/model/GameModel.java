package app.model;

import app.pattern.Observable;

import java.util.ArrayList;
import java.util.List;

public class GameModel extends Observable {

    private List<PlayerModel> playerModelList;
    private List<TileModel> tileModelList;
    private WatterLevelModel watterLevelModel;

    public GameModel() {
        this.playerModelList = new ArrayList<>();
        this.watterLevelModel = new WatterLevelModel();
        this.tileModelList = new ArrayList<>();
    }

    public List<PlayerModel> getPlayers() {
        return playerModelList;
    }

    public void setPlayers(List<PlayerModel> playerModels) {
        this.playerModelList = playerModels;
    }

    public WatterLevelModel getWatterLevelModel() {
        return watterLevelModel;
    }

    public void setWatterLevelModel(WatterLevelModel watterLevelModel) {
        this.watterLevelModel = watterLevelModel;
    }

    public List<TileModel> getTileModelList() {
        return tileModelList;
    }

    public void setTileModelList(List<TileModel> tileModelList) {
        this.tileModelList = tileModelList;
    }
}