package app.model;

import app.pattern.Observable;

public class WatterLevelModel extends Observable {
    private int watterLevel;

    public int getWatterLevel() {
        return watterLevel;
    }

    public void setWatterLevel(int watterLevel) {
        this.watterLevel = watterLevel;
    }
}