package app.model;

import app.enums.Role;

public class PlayerModel {
    private String name;
    private Role role;
    private int actionCount;

    public PlayerModel(String name, Role role) {
        this.actionCount = 3;
        this.name = name;
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public int getActionCount() {
        return actionCount;
    }

    public void setActionCount(int actionCount) {
        this.actionCount = actionCount;
    }

    @Override
    public String toString() {
        return "PlayerModel{" +
                "name='" + name + '\'' +
                ", role=" + role +
                ", actionCount=" + actionCount +
                '}';
    }
}