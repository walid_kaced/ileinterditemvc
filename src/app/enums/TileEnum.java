package app.enums;

public enum TileEnum {
    // Liste des Tuils asséchées
    LaPortedArgent("La Porte d'Argent","src/assets/tile/LaPortedArgent.png", TileStatus.DRIED_UP),
    LaPorteDeBronze("La Porte de Bronze","src/assets/tile/LaPorteDeBronze.png", TileStatus.DRIED_UP),
    LaPorteDeCuivre("La Porte de Cuivre","src/assets/tile/LaPorteDeCuivre.png", TileStatus.DRIED_UP),
    LesDunesDeLIllusion("Les Dunes de l'Illusion", "src/assets/tile/LesDunesDeLIllusion.png", TileStatus.DRIED_UP),
    LesFalaisesDeLOubli("Les Falaises de l'Oubli","src/assets/tile/LesFalaisesDeLOubli.png", TileStatus.DRIED_UP),
    LaForetPourpre("La Forêt Pourpre","src/assets/tile/LaForetPourpre.png", TileStatus.DRIED_UP),
    LaCarverneDesOmbres("La Caverne des Ombres","src/assets/tile/LaCarverneDesOmbres.png", TileStatus.DRIED_UP),
    LaCarverneDuBrasier("La Caverne du Brasier","src/assets/tile/LaCarverneDuBrasier.png", TileStatus.DRIED_UP),
    Heliport("L'Héliport","src/assets/tile/Heliport.png", TileStatus.DRIED_UP),
    LeJardinDesHurlements("Le Jardin des Hurlements","src/assets/tile/LeJardinDesHurlements.png", TileStatus.DRIED_UP),
    LeJardinDesMurmures("Le Jardin des Murmures","src/assets/tile/LeJardinDesMurmures.png", TileStatus.DRIED_UP),
    LeLagonPerdu("Le Lagon Perdu","src/assets/tile/LeLagonPerdu.png", TileStatus.DRIED_UP),
    LeMaraisBrumeux("Le Marais Brumeux","src/assets/tile/LeMaraisBrumeux.png", TileStatus.DRIED_UP),
    Observatoire("L'Observatoire","src/assets/tile/Observatoire.png", TileStatus.DRIED_UP),
    LePalaisDeCorail("Le Palais de Corail","src/assets/tile/LePalaisDeCorail.png", TileStatus.DRIED_UP),
    LePalaisDesMarees("Le Palais des Marées","src/assets/tile/LePalaisDesMarees.png", TileStatus.DRIED_UP),
    LePontDesAbimes("Le Pont des Abîmes","src/assets/tile/LePontDesAbimes.png", TileStatus.DRIED_UP),
    LaPorteDeFer("La Porte de Fer","src/assets/tile/LaPorteDeFer.png", TileStatus.DRIED_UP),
    LaPorteDOr("La Porte d'Or","src/assets/tile/LaPorteDOr.png", TileStatus.DRIED_UP),
    LeRocherFantome("Le Rocher Fantôme","src/assets/tile/LeRocherFantome.png", TileStatus.DRIED_UP),
    LeTempleDeLaLune("Le Temple de la Lune","src/assets/tile/LeTempleDeLaLune.png", TileStatus.DRIED_UP),
    LeTempleDuSoleil("Le Temple du Soleil","src/assets/tile/LeTempleDuSoleil.png", TileStatus.DRIED_UP),
    LaTourDuGuet("La Tour de Guet","src/assets/tile/LaTourDuGuet.png", TileStatus.DRIED_UP),
    LeValDuCrepuscule("Le Val du Crépuscule","src/assets/tile/LeValDuCrepuscule.png", TileStatus.DRIED_UP),
    Empty("Carte Vide","src/assets/tile/empty.png", TileStatus.CASTED),

    // Liste des Tuils inondées
    LaPortedArgent_flooded("La Porte d'Argent","src/assets/tile/LaPortedArgent_flooded.png", TileStatus.FLOODED),
    LaPorteDeBronze_flooded("La Porte de Bronze","src/assets/tile/LaPorteDeBronze_flooded.png", TileStatus.FLOODED),
    LaPorteDeCuivre_flooded("La Porte de Cuivre","src/assets/tile/LaPorteDeCuivre_flooded.png", TileStatus.FLOODED),
    LesDunesDeLIllusion_flooded("Les Dunes de l'Illusion", "src/assets/tile/LesDunesDeLIllusion_flooded.png", TileStatus.FLOODED),
    LesFalaisesDeLOubli_flooded("Les Falaises de l'Oubli","src/assets/tile/LesFalaisesDeLOubli_flooded.png", TileStatus.FLOODED),
    LaForetPourpre_flooded("La Forêt Pourpre","src/assets/tile/LaForetPourpre_flooded.png", TileStatus.FLOODED),
    LaCarverneDesOmbres_flooded("La Caverne des Ombres","src/assets/tile/LaCarverneDesOmbres_flooded.png", TileStatus.FLOODED),
    LaCarverneDuBrasier_flooded("La Caverne du Brasier","src/assets/tile/LaCarverneDuBrasier_flooded.png", TileStatus.FLOODED),
    Heliport_flooded("L'Héliport","src/assets/tile/Heliport_flooded.png", TileStatus.FLOODED),
    LeJardinDesHurlements_flooded("Le Jardin des Hurlements","src/assets/tile/LeJardinDesHurlements_flooded.png", TileStatus.FLOODED),
    LeJardinDesMurmures_flooded("Le Jardin des Murmures","src/assets/tile/LeJardinDesMurmures_flooded.png", TileStatus.FLOODED),
    LeLagonPerdu_flooded("Le Lagon Perdu","src/assets/tile/LeLagonPerdu_flooded.png", TileStatus.FLOODED),
    LeMaraisBrumeux_flooded("Le Marais Brumeux","src/assets/tile/LeMaraisBrumeux_flooded.png", TileStatus.FLOODED),
    Observatoire_flooded("L'Observatoire","src/assets/tile/Observatoire_flooded.png", TileStatus.FLOODED),
    LePalaisDeCorail_flooded("Le Palais de Corail","src/assets/tile/LePalaisDeCorail_flooded.png", TileStatus.FLOODED),
    LePalaisDesMarees_flooded("Le Palais des Marées","src/assets/tile/LePalaisDesMarees_flooded.png", TileStatus.FLOODED),
    LePontDesAbimes_flooded("Le Pont des Abîmes","src/assets/tile/LePontDesAbimes_flooded.png", TileStatus.FLOODED),
    LaPorteDeFer_flooded("La Porte de Fer","src/assets/tile/LaPorteDeFer_flooded.png", TileStatus.FLOODED),
    LaPorteDOr_flooded("La Porte d'Or","src/assets/tile/LaPorteDOr_flooded.png", TileStatus.FLOODED),
    LeRocherFantome_flooded("Le Rocher Fantôme","src/assets/tile/LeRocherFantome_flooded.png", TileStatus.FLOODED),
    LeTempleDeLaLune_flooded("Le Temple de la Lune","src/assets/tile/LeTempleDeLaLune_flooded.png", TileStatus.FLOODED),
    LeTempleDuSoleil_flooded("Le Temple du Soleil","src/assets/tile/LeTempleDuSoleil_flooded.png", TileStatus.FLOODED),
    LaTourDuGuet_flooded("La Tour de Guet","src/assets/tile/LaTourDuGuet_flooded.png", TileStatus.FLOODED),
    LeValDuCrepuscule_flooded("Le Val du Crépuscule","src/assets/tile/LeValDuCrepuscule_flooded.png", TileStatus.FLOODED);

    private String wording;
    private String pathImage;
    private TileStatus tileStatus;

    TileEnum(String wording, String pathImage,TileStatus tileStatus ) {
        this.wording = wording;
        this.pathImage = pathImage;
        this.tileStatus = tileStatus;
    }

    public TileStatus getTileStatus() {
        return tileStatus;
    }

    public void setTileStatus(TileStatus tileStatus) {
        this.tileStatus = tileStatus;
    }

    public String getPathImage() {
        return pathImage;
    }

    public void setPathImage(String pathImage) {
        this.pathImage = pathImage;
    }
}
