package app.enums;

public enum TileStatus {
    FLOODED("Inondée"),
    DRIED_UP("Asséchée"),
    CASTED("Coulée");

    private final String wording ;

    TileStatus(String wording) {
        this.wording = wording ;
    }
}
