package app.enums;

public enum Role {
    Ingénieur("Ingénieur"),
    Pilote("Pilote"),
    Explorateur("Explorateur"),
    Navigateur("Navigateur"),
    Plongeur("Plongeur"),
    Messager("Messager");

    private final String wording ;

    Role(String wording) {
        this.wording = wording ;
    }
}
