package app.enums;

public enum Treasure {
    CALICE("Le Calice de l'Onde", "src/assets/treasure/calice.png"),
    PIERRE("La Pierre Sacrée", "src/assets/treasure/pierre.png"),
    CRISTAL("Le Cristal Ardent", "src/assets/treasure/cristal.png"),
    ZEPHYR("La statue du Zéphyr", "src/assets/treasure/zephyr.png");

    private String wording;
    private String pathImage;

    Treasure(String wording, String pathImage) {
        this.wording = wording;
        this.pathImage = pathImage;
    }

    public String getPathImage() {
        return pathImage;
    }

    public void setPathImage(String pathImage) {
        this.pathImage = pathImage;
    }
}