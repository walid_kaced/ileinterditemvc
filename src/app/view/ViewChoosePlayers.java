package app.view;

import app.controller.Controller;
import app.pattern.Observer;
import app.view.improved.ViewButton;
import app.view.improved.ViewWindowFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

public class ViewChoosePlayers extends ViewWindowFrame implements Observer {
    private ViewButton[] buttonGameLevels = {new ViewButton("Novice", 0, 35), new ViewButton("Intermédiaire", 0, 35), new ViewButton("Élite", 0, 35), new ViewButton("Légendaire", 0, 35)};
    private JTextField[] textFieldsNamesPlayers = new JTextField[4];
    private JComboBox comboBoxNumberOfPlayers = new JComboBox<>(new Integer[]{2, 3, 4});

    public ViewChoosePlayers(Controller controller) {
        super("Choix des joueurs");
        this.setSize(500, 300);

        // Panel South
        JPanel panelSouth = new JPanel(new BorderLayout());
        Component componentGameLevel = new JLabel("Choix du niveau de la partie", SwingConstants.CENTER);
        componentGameLevel.setFont(new Font("TimesRoman", Font.BOLD, 15));
        panelSouth.add(componentGameLevel, BorderLayout.NORTH);
        JPanel panelLevel = new JPanel(new GridLayout(1, 4));
        panelSouth.add(panelLevel, BorderLayout.CENTER);

        for (ViewButton buttonGameLevel : buttonGameLevels) {
            panelLevel.add(buttonGameLevel);
            buttonGameLevel.addActionListener(controller);
        }
        this.changeButtonColor(buttonGameLevels[0]);

        // Panel North
        JPanel panelNorth = new JPanel();
        panelNorth.setLayout(new BorderLayout());
        ViewButton exitButton = new ViewButton("Quitter");
        panelNorth.add(exitButton, BorderLayout.WEST);
        ViewButton playButton = new ViewButton("Jouer");
        panelNorth.add(playButton, BorderLayout.EAST);

        // Panel Center
        JPanel panelNumberOfPlayers = this.fillGamePlayers();

        this.getPanelView().setLayout(new BorderLayout());
        this.getPanelView().add(panelSouth, BorderLayout.SOUTH);
        this.getPanelView().add(panelNumberOfPlayers, BorderLayout.CENTER);
        this.getPanelView().add(panelNorth, BorderLayout.NORTH);

        // Ajout du controleur pour controler les actions des boutons
        playButton.addActionListener(controller);
        exitButton.addActionListener(controller);

        this.setLocationRelativeTo(null);
    }

    private JPanel fillGamePlayers() {
        JLabel[] labelNamePlayers = new JLabel[4];
        JPanel panelNumberOfPlayers = new JPanel(new GridLayout(5, 2));
        panelNumberOfPlayers.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0, 0), 5));

        panelNumberOfPlayers.add(new JLabel("Choix du nombre de joueurs :"));
        panelNumberOfPlayers.add(comboBoxNumberOfPlayers);

        // Initialisation des champs de saisie des noms de joueurs
        for (int i = 0; i < 4; i++) {
            labelNamePlayers[i] = new JLabel("Joueur " + (i + 1) + " :");
            textFieldsNamesPlayers[i] = new JTextField();
            panelNumberOfPlayers.add(labelNamePlayers[i]);
            panelNumberOfPlayers.add(textFieldsNamesPlayers[i]);
            labelNamePlayers[i].setEnabled(i < 2);
            textFieldsNamesPlayers[i].setEnabled(i < 2);
        }
        // Activation des champs de saisie
        comboBoxNumberOfPlayers.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                int numberOfPlayersSelected = (Integer) comboBoxNumberOfPlayers.getSelectedItem();

                for (int i = 0; i < textFieldsNamesPlayers.length; i++) {
                    labelNamePlayers[i].setEnabled(i < numberOfPlayersSelected);
                    textFieldsNamesPlayers[i].setEnabled(i < numberOfPlayersSelected);
                }
            }
        });

        return panelNumberOfPlayers;
    }

    public void resetButtonsColor() {
        for (ViewButton buttonGameLevel : this.buttonGameLevels) {
            buttonGameLevel.setColors(Color.GRAY, Color.white);
            buttonGameLevel.repaint();
        }
    }

    public void changeButtonColor(ViewButton btn) {
        btn.setColors(Color.DARK_GRAY, Color.LIGHT_GRAY);
    }

    public JTextField[] getTextFieldsNamesPlayers() {
        return textFieldsNamesPlayers;
    }

    public JComboBox getComboBoxNumberOfPlayers() {
        return comboBoxNumberOfPlayers;
    }

    @Override
    public void update(Object object) {
        this.resetButtonsColor();
        ActionEvent event = (ActionEvent) object;
        this.changeButtonColor((ViewButton) event.getSource());
    }
}