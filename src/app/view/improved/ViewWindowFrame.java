package app.view.improved;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class ViewWindowFrame extends JFrame {
    private JPanel panelView;
    private ViewWindowFrame frame = this;

    public ViewWindowFrame(String title) {
        JLabel titleLabel = new JLabel(title);
        titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
        titleLabel.setForeground(Color.BLACK);
        titleLabel.setFont(new Font("TimesRoman",Font.BOLD,20));
        makeMouvablePanel(titleLabel);

        panelView = new JPanel(new FlowLayout());
        panelView.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0, 0), 10));

        JPanel contentPanel = new JPanel(new GridLayout(1, 1));
        contentPanel.add(panelView);

        this.getContentPane().setBackground(Color.GRAY);
        this.setLayout(new BorderLayout());
        this.add(titleLabel, BorderLayout.NORTH);
        this.add(contentPanel, BorderLayout.CENTER);
        this.setUndecorated(true);
        this.setVisible(true);
    }

    private void makeMouvablePanel(JComponent panel) {
        MoveListener listener = new MoveListener();
        panel.addMouseListener(listener);
        panel.addMouseMotionListener(listener);
    }

    public JPanel getPanelView() {
        return panelView;
    }

    public void setPanelView(JPanel panelView) {
        this.panelView = panelView;
    }

    // Une classe inner pour aider à faire bouger le Panel avec la souris
    private class MoveListener implements MouseListener, MouseMotionListener {

        private Point pressedPoint;
        private Rectangle frameBounds;

        @Override
        public void mouseClicked(MouseEvent event) {
        }

        @Override
        public void mousePressed(MouseEvent event) {
            this.frameBounds = frame.getBounds();
            this.pressedPoint = event.getPoint();
        }

        @Override
        public void mouseReleased(MouseEvent event) {
            moveJFrame(event);
        }

        @Override
        public void mouseEntered(MouseEvent event) {
        }

        @Override
        public void mouseExited(MouseEvent event) {
        }

        @Override
        public void mouseDragged(MouseEvent event) {
            moveJFrame(event);
        }

        @Override
        public void mouseMoved(MouseEvent event) {
        }

        private void moveJFrame(MouseEvent event) {
            Point endPoint = event.getPoint();

            int xDiff = endPoint.x - pressedPoint.x;
            int yDiff = endPoint.y - pressedPoint.y;
            frameBounds.x += xDiff;
            frameBounds.y += yDiff;
            frame.setBounds(frameBounds);
        }
    }
}
