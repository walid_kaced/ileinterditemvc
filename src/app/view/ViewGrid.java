package app.view;

import app.enums.TileEnum;
import app.enums.TileStatus;
import app.enums.Treasure;
import app.model.GameModel;
import app.util.RandomValuesEnum;
import app.view.improved.ViewWindowFrame;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class ViewGrid extends ViewWindowFrame {
    private List<TileEnum> tileEnumList;

    public ViewGrid() {
        super("Jeux île interdite");
        this.setSize(1000, 1000);

        initGrid();
        setVisible(true);
    }

    public void initGrid() {
        JPanel panel = new JPanel(new GridLayout(6,6));
        TileEnum tile;
        ImageIcon imageIcon;
        Treasure treasure;
        int index = 0;

        tileEnumList = new RandomValuesEnum().getRandomListTileEnum(TileStatus.DRIED_UP);

        for (int i=0; i<6; i++) {
            for (int j=0; j<6; j++) {
                if ((i==0 && j==0)) {
                    treasure = Treasure.CRISTAL;
                    imageIcon = new ImageIcon(new ImageIcon(treasure.getPathImage()).getImage().getScaledInstance(150, 150, Image.SCALE_DEFAULT));
                } else if ((i==0 && j==5)) {
                    treasure = Treasure.CALICE;
                    imageIcon = new ImageIcon(new ImageIcon(treasure.getPathImage()).getImage().getScaledInstance(150, 150, Image.SCALE_DEFAULT));
                } else if ((i==5 && j==0)) {
                    treasure = Treasure.PIERRE;
                    imageIcon = new ImageIcon(new ImageIcon(treasure.getPathImage()).getImage().getScaledInstance(150, 150, Image.SCALE_DEFAULT));
                } else if ((i==5 && j==5)) {
                    treasure = Treasure.ZEPHYR;
                    imageIcon = new ImageIcon(new ImageIcon(treasure.getPathImage()).getImage().getScaledInstance(150, 150, Image.SCALE_DEFAULT));
                } else if ((i==0 && j==1) || (i==0 && j==4) || (i==1 && j==0) || (i==1 && j==5) || (i==4 && j==0) || (i==4 && j==5) || (i==5 && j==1) || (i==5 && j==4) ) {
                    tile = TileEnum.Empty;
                    imageIcon = new ImageIcon(new ImageIcon(tile.getPathImage()).getImage().getScaledInstance(150, 150, Image.SCALE_DEFAULT));
                }
                else {
                    tile = tileEnumList.get(index);
                    index++;
                    imageIcon = new ImageIcon(new ImageIcon(tile.getPathImage()).getImage().getScaledInstance(150, 150, Image.SCALE_DEFAULT));
                }
                JLabel label = new JLabel(imageIcon);
                label.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0, 0), 3));
                panel.add(label);
            }
        }
        this.getPanelView().add(panel, BorderLayout.CENTER);

        this.setLocationRelativeTo(null);
    }

    public List<TileEnum> getTileEnumList() {
        return tileEnumList;
    }

    public void setTileEnumList(List<TileEnum> tileEnumList) {
        this.tileEnumList = tileEnumList;
    }
}