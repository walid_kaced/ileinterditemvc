package app.view;

import app.model.WatterLevelModel;
import app.view.improved.ViewWindowFrame;

import javax.swing.*;
import java.awt.*;

public class ViewWaterLevel extends ViewWindowFrame {
    WatterLevelModel watterLevelModel;

    public ViewWaterLevel() {
        super("Niveau d'eau");
        this.setBounds(100, 20, 310, 600);

        initWaterLevel();
        setVisible(true);
    }

    private void initWaterLevel() {
        JPanel panel = new JPanel(new GridLayout(1,1));
        String pathImage = "src/assets/watterLevel.png";
        ImageIcon imageIcon = new ImageIcon(new ImageIcon(pathImage).getImage().getScaledInstance(200, 500, Image.SCALE_DEFAULT));

        JLabel label = new JLabel(imageIcon);
        label.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0, 0), 3));
        panel.add(label);

        this.getPanelView().add(panel, BorderLayout.CENTER);
    }
}