package app.view;

import app.controller.Controller;
import app.pattern.Observer;
import app.view.improved.ViewButton;
import app.view.improved.ViewWindowFrame;

import java.awt.*;

public class ViewPlayerPanel extends ViewWindowFrame implements Observer {

    public ViewPlayerPanel(Controller controller) {
        super("Manette du joueur");
        this.setBounds(1500, 20, 300, 100);

        ViewButton endOfTurnButton = new ViewButton("Fin de tour");
        endOfTurnButton.addActionListener(controller);

        this.getPanelView().add(endOfTurnButton, BorderLayout.CENTER);
    }

    @Override
    public void update(Object object) {

    }
}