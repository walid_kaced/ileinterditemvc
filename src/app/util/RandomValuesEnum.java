package app.util;

import app.enums.Role;
import app.enums.TileEnum;
import app.enums.TileStatus;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class RandomValuesEnum {
    private List<TileEnum> tileEnumList = new ArrayList<>();
    private List<Role> roleList = new ArrayList<>();

    // Récupère une liste de valeurs non doublées de l'enum TileEnum avec une condition de la valeur de TileStatus donnée
    public List<TileEnum> getRandomListTileEnum(TileStatus enumCondition) {
        List<TileEnum> shuffled = Arrays.stream(TileEnum.values()).filter(tileEnum -> tileEnum.getTileStatus() == enumCondition).collect(Collectors.toList());
        Collections.shuffle(shuffled);
        for (TileEnum tile : shuffled) {
            this.tileEnumList.add(tile);
        }

        return this.tileEnumList;
    }

    // Récupère une liste de valeurs non doublées de l'enum Role
    public List<Role> getRandomListRoleEnum() {
        List<Role> shuffled = Arrays.asList(Role.values());
        Collections.shuffle(shuffled);
        for (Role role : shuffled) {
            this.roleList.add(role);
        }

        return this.roleList;
    }
}