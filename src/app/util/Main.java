package app.util;

import app.controller.Controller;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
	EventQueue.invokeLater(() -> {
        new Controller();
	    });
    }
}