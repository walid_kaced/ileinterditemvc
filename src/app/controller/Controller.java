package app.controller;

import app.enums.Role;
import app.model.GameModel;
import app.model.PlayerModel;
import app.util.RandomValuesEnum;
import app.view.ViewChoosePlayers;
import app.view.ViewGrid;
import app.view.ViewPlayerPanel;
import app.view.ViewWaterLevel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class Controller implements ActionListener {
    GameModel gameModel;
    ViewChoosePlayers viewChoosePlayers;
    ViewGrid viewGrid;
    ViewWaterLevel viewWaterLevel;
    ViewPlayerPanel viewPlayerPanel;

    public Controller() {
        this.gameModel = new GameModel();
        this.viewChoosePlayers = new ViewChoosePlayers(this);
        // Ajout des observables
        this.gameModel.getWatterLevelModel().addObserver(this.viewChoosePlayers);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // Controls de la vue ViewChoosePlayers
        if (e.getActionCommand().equalsIgnoreCase("jouer")) {
            // mise à jour des informations des joueurs
            int numberOfPlayers = (int) this.viewChoosePlayers.getComboBoxNumberOfPlayers().getSelectedItem();
            List<Role> roleList = new RandomValuesEnum().getRandomListRoleEnum();
            for (int i = 0; i < numberOfPlayers; ++i) {
                PlayerModel player = new PlayerModel(this.viewChoosePlayers.getTextFieldsNamesPlayers()[i].getText(), roleList.get(i));
                this.gameModel.getPlayers().add(player);
            }
            // Faire disparaitre la vue des choix de joueurs
            this.viewChoosePlayers.dispose();
            // Faire apparaitre les vues du jeux
            this.viewGrid = new ViewGrid();
            this.viewWaterLevel = new ViewWaterLevel();
            this.viewPlayerPanel = new ViewPlayerPanel(this);
        } else if (e.getActionCommand().equalsIgnoreCase("quitter")) {
            System.exit(0);
        } else if (e.getActionCommand().equalsIgnoreCase("novice")) {
            this.gameModel.getWatterLevelModel().setWatterLevel(0);
            // Notification des Observeurs
            this.gameModel.getWatterLevelModel().notifyObservers(e);
        } else if (e.getActionCommand().equalsIgnoreCase("Intermédiaire")) {
            this.gameModel.getWatterLevelModel().setWatterLevel(1);
            // Notification des Observeurs
            this.gameModel.getWatterLevelModel().notifyObservers(e);
        } else if (e.getActionCommand().equalsIgnoreCase("Élite")) {
            this.gameModel.getWatterLevelModel().setWatterLevel(2);
            // Notification des Observeurs
            this.gameModel.getWatterLevelModel().notifyObservers(e);
        } else if (e.getActionCommand().equalsIgnoreCase("Légendaire")) {
            this.gameModel.getWatterLevelModel().setWatterLevel(3);
            // Notification des Observeurs
            this.gameModel.getWatterLevelModel().notifyObservers(e);
        }
        // Controls de la vue ViewPlayerPanel
        else if (e.getActionCommand().equalsIgnoreCase("Fin de tour")){
            // TODO: mise à jour du model et notification de la vue
            System.out.println("fin du tour");
        }
    }
}